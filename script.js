'use strict';

const movieDB = {
	movies: [
		"Логан",
		"Лига справедливости",
		"Гарри Поттер",
		
		
	]
};

// Доступ к элементам 
const  movieFilms = document.querySelector(".promo__interactive-list"),  // Список с фильмами
	addForm = document.querySelector('form.add'), // Форма -> input
	addInput = addForm.querySelector('.adding__input'),
	checkbox = addForm.querySelector('[type="checkbox"]'); // input - type="checkbox"

// Обработчик события для Формы - Срабатывает при нажатии кнопки "Подтвердить"
// Здесь будут реализованы ЗАДАНИЯ №1, №2 и №4
addForm.addEventListener('submit', (event) => {
	event.preventDefault(); // Страница не перегружается - Отмена станд. поведения

	let newFilm = addInput.value; // Название фильма, которое вводится в поле 
	const favorite = checkbox.checked; // Атрибут checked: определяет - есть ли флажок?

	if (newFilm) { // Проверка на пустую строку. Введено ли название фильма? 

        // ЗАДАНИЕ №2
        if (newFilm.length > 21) {
			newFilm = `${newFilm.substring(0,22)}...`;
		}

        // ЗАДАНИЕ №4
		if (favorite) {
			console.log('Добавляем любимый фильм');
		}

        // ЗАДАНИЕ №1
		movieDB.movies.push(newFilm); // Добавляем новый фильм в БАЗУ movieDB.movies
        // Перебор фильмов в БАЗЕ и добавление их в movieFilms - список на странице         
		createMovieList(movieDB.movies, movieFilms); 
	}

	// addForm.reset(); - Очистка формы. Здесь addForm меняем на event.target
	event.target.reset();
});




const sortArr = (arr) => {
	arr.sort();
};



// Функция createMovieList 
// 1. Перебор фильмов и добавление фильма на страницу в movieFilms
// 2. Удаление фильма при клике на корзину
// films - массив с фильмами: в д.сл. БАЗА movieDB.movies
// parent - блок на странице, в который помещаются фильмы: в д.сл. movieFilms
function createMovieList(films, parent) {
	
	parent.innerHTML = ""; // Удаляем начальный список с фильмами (весь код в блоке movieFilms)
    // ЗАДАНИЕ №5 - Сортировка фильмов 
    sortArr(films);        

    // 1. Перебор и добавление фильмов в movieFilms.
	films.forEach((film, i) => {
		parent.innerHTML += `
             <li class="promo__interactive-item">${i + 1}. ${film}
            <div class="delete"><i class="fa fa-trash" aria-hidden="true"></i></div>
        </li>`;
	});

    // 2. Удаление фильма при клике на корзину - ЗАДАНИЕ №3
    // Получаем доступ ко всем корзинам и сразу же перебираем их...
    document.querySelectorAll('.delete').forEach((btn, i) => {
        // Обработчик для каждой корзины - срабатывает при клике на ней
        btn.addEventListener('click', () => {
			// 1. При клике удаляется родительский элемент со страницы: элемент списка li, т.е. фильм и ....
			btn.parentElement.remove();
			movieDB.movies.splice(i, 1); // ... 2. соответствующий элемент массива/фильм из БАЗЫ

			// Рекурсия - вызываем функцию внутри себя же - чтобы после удаления фильма, шла их пере нумерация
			createMovieList(films, parent);
		});
	});
}


createMovieList(movieDB.movies, movieFilms);